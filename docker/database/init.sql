SET NAMES utf8;
SET NAMES utf8mb4;
CREATE DATABASE IF NOT EXISTS `test`  DEFAULT CHARACTER SET utf8 ;
USE `test`;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` double DEFAULT 0,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user` (`id`, `name`, `login`, `balance`, `password`) VALUES
(1, 'DemoUser', 'demo', 5000, '$2y$10$4D3gpGTst556S3mgQWsAruFZ1fwO9b37WLI/pQzNHzcP45xQ2MzWa');

