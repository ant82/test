<?php

use App\Controller\IndexController;
use Symfony\Component\HttpFoundation\Response;

require __DIR__ . '/../vendor/autoload.php';

$container = require __DIR__ . '/../boot/boot.php';

$dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/', [IndexController::class, 'dashboard']);
    $r->addRoute('GET', '/dashboard', [IndexController::class, 'dashboard']);
    $r->addRoute('POST', '/transaction', [IndexController::class, 'transaction']);
    // {id} must be a number (\d+)
    $r->addRoute('GET', '/login', [IndexController::class, 'login']);
    $r->addRoute('POST', '/login', [IndexController::class, 'login']);
    $r->addRoute('POST', '/logout', [IndexController::class, 'logout']);
    // The /{title} suffix is optional
    $r->addRoute('GET', '/articles/{id:\d+}[/{title}]', 'get_article_handler');
});

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri        = $_SERVER['REQUEST_URI'];

if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        $response = new Response(
            'HTTP_NOT_FOUND',
            Response::HTTP_NOT_FOUND,
            ['content-type' => 'text/html']
        );
        $response->send();
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        $response       = new Response(
            'HTTP_METHOD_NOT_ALLOWED',
            Response::HTTP_METHOD_NOT_ALLOWED,
            ['content-type' => 'text/html']
        );
        $response->send();
        break;
    case FastRoute\Dispatcher::FOUND:
        $controller = $routeInfo[1];
        $parameters = $routeInfo[2];
        /** @var Response $response */
        $response = $container->call($controller, $parameters);
        $response->send();
        break;
}
