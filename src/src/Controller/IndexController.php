<?php

namespace App\Controller;

use App\Service\TransactionService;
use App\Service\Validator\Login\LoginFormValidator;
use App\Service\Validator\Transaction\TransactionValidator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class IndexController extends AbstractController
{

    /**
     * IndexController constructor.
     * @param Session $session
     * @return RedirectResponse
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */

    public function dashboard(Session $session): Response
    {
        if (!$this->auth->isAuth()) {
            return new RedirectResponse('/login');
        }

        $okMsg  = $session->getFlashBag()->get('okMsg');
        $errors = $session->getFlashBag()->get('errors');
        $html   = $this->twig->render('dashboard.html.twig',
            [
                'user'   => $this->auth->getUser(),
                'errors' => $errors,
                'okMsg'  => $okMsg
            ]);

        return new Response($html);

    }

    public function transaction(TransactionValidator $transactionValidator,
                                TransactionService $transactionService,
                                Session $session)
    {
        $data           = $this->request->request->all();
        $transactionDto = $transactionValidator->validate($data);
        $errors         = $transactionValidator->getErrors();
        if ($transactionDto) {
            if (!$transactionService->makeTransaction($transactionDto)) {
                $errors[] = 'Ошибка проведения транзакции';
            }
        }

        $session->getFlashBag()->set('errors', $errors);
        if (empty($errors)) {
            $session->getFlashBag()->set('okMsg', 'Транзакция успешно проведена');
        }

        return new RedirectResponse('/dashboard');
    }

    public function login(LoginFormValidator $loginFormValidator): Response
    {
        if ($this->auth->isAuth()) {
            return new RedirectResponse('/dashboard');
        }

        $errors = [];
        if ($this->request->isMethod('POST')) {
            $data     = $this->request->request->get('auth');
            $loginDto = $loginFormValidator->validate($data);
            $errors   = $loginFormValidator->getErrors();
            if ($loginDto) {
                if ($this->auth->login($loginDto)) {
                    return new RedirectResponse('/dashboard');
                }
                $errors[] = 'Неправильный логин или пароль';
            }
        }
        $html = $this->twig->render('login.html.twig', ['errors' => $errors]);

        return new Response($html);

    }

    public function logout(): Response
    {

        $this->auth->logout();

        return new RedirectResponse('/login');
    }

}