<?php

namespace App\Controller;

use App\Service\Auth;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

abstract class AbstractController
{
    /**
     * @var Auth
     */
    protected $auth;
    /**
     * @var Environment
     */
    protected $twig;
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Auth $auth, Environment $twig,Request $request)
    {
        $this->auth = $auth;
        $this->twig = $twig;
        $this->request = $request;
    }
}