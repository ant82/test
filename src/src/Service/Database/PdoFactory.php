<?php

namespace App\Service\Database;

use PDO;

class PdoFactory
{
    private static $pdo;

    /**
     * PdoFactory constructor.
     */
    private function __construct()
    {
    }

    public static function getPdo(): \PDO
    {
        if (!self::$pdo) {
            $host    = getenv('DB_HOST');
            $db      = getenv('DB_DATABASE');
            $user    = getenv('DB_USER');
            $pass    = getenv('DB_PASSWORD');
            $dsn = "mysql:host=$host;dbname=$db;charset=utf8";
            $opt = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            self::$pdo = new PDO($dsn, $user, $pass, $opt);
        }

        return self::$pdo;
    }


}