<?php

namespace App\Service;

use App\Repository\UserRepositoryInterface;
use App\Service\Validator\Transaction\TransactionDTO;

class TransactionService
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * TransactionService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function makeTransaction(TransactionDTO $transactionDTO): bool
    {
        try {
            $this->userRepository->makeTransaction($transactionDTO->getUser(), $transactionDTO->getSum());
        } catch (\Throwable $throwable) {
            var_dump($throwable->getMessage());
            // todo: log error
            return false;

        }

        return true;
    }


}