<?php

namespace App\Service\Factory;

use App\Entity\User;

class UserFactory
{
    public function createFromArray(array $data): User
    {
        $user = new User();
        foreach ($data as $k => $v) {
            switch ($k) {
                case 'id':
                    $user->setId($v);
                    break;
                case 'name':
                    $user->setName($v);
                    break;
                case 'balance':
                    $user->setBalance((float)$v);
                    break;
                case 'login':
                    $user->setLogin($v);
                    break;
                case 'password':
                    $user->setPassword($v);
                    break;
            }
        }

        return $user;
    }

}