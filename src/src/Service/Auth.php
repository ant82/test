<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepositoryInterface;
use App\Service\Validator\Login\LoginFormDTO;
use Symfony\Component\HttpFoundation\Request;

class Auth
{
    const SESSION_KEY = 'user';
    const SESSION_CSRF_TOKEN = 'csrf-token';
    /**
     * @var Request
     */
    private $request;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * Auth constructor.
     * @param Request $request
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(Request $request, UserRepositoryInterface $userRepository)
    {
        $this->request        = $request;
        $this->userRepository = $userRepository;
        $this->init();
    }

    public function login(LoginFormDTO $loginFormDTO): bool
    {
        $user = $this->userRepository->getUserByLogin($loginFormDTO->getLogin());
        if ($user) {
            if (password_verify($loginFormDTO->getPassword(), $user->getPassword())) {
                $user->setCSRFToken($this->generateCSRFToken());
                $this->user = $user;
                $this->request->getSession()->set(self::SESSION_KEY, $user->getId());
                $this->request->getSession()->set(self::SESSION_CSRF_TOKEN, $user->getCSRFToken());

                return true;
            }
        }

        return false;
    }

    public function register()
    {
        //$user->setPassword(password_hash(trim($data['password']), PASSWORD_DEFAULT));
    }

    public function logout(): void
    {
        //$this->request->getSession()->remove(self::SESSION_KEY);
        $this->request->getSession()->clear();
    }

    private function init()
    {
        $userId = $this->request->getSession()->get(self::SESSION_KEY);
        if ($userId) {
            $user = $this->userRepository->getUserById($userId);
            if ($user instanceof User) {
                $user->setCSRFToken($this->request->getSession()->get(self::SESSION_CSRF_TOKEN));
                $this->user = $user;
            }
        }
        $this->request->getSession()->save();
    }

    public function isAuth(): bool
    {
        return $this->user instanceof User;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    private function generateCSRFToken()
    {
        return bin2hex(openssl_random_pseudo_bytes(16));
    }

}