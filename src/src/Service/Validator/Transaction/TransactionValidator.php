<?php

namespace App\Service\Validator\Transaction;

use App\Service\Auth;
use Symfony\Component\HttpFoundation\Session\Session;

class TransactionValidator
{
    private $errors = [];
    /**
     * @var Auth
     */
    private $auth;

    /**
     * TransactionValidator constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {

        $this->auth = $auth;
    }

    public function validate(array $data): ?TransactionDTO
    {
        if (empty($data['csrf'])) {
            $this->errors['csrf'] = 'Empty token';
        }
        else if ($data['csrf'] != $this->auth->getUser()->getCSRFToken()) {
            $this->errors['csrf'] = 'Incorrect token';
        }
        if (empty($data['sum']) || (float)$data['sum'] === 0.0) {
            $this->errors['sum'] = 'Empty transaction sum';
        }
        elseif ((float)$data['sum'] > $this->auth->getUser()->getBalance()) {
            $this->errors['sum'] = 'Transaction sum greater than balance';
        }

        if (empty($this->errors)) {
            return new TransactionDTO((float)$data['sum'], $this->auth->getUser());
        }

        return null;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

}