<?php

namespace App\Service\Validator\Transaction;

use App\Entity\User;

class TransactionDTO
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var int
     */
    private $sum;

    /**
     * LoginFormDTO constructor.
     * @param $sum
     * @param User $user
     */
    public function __construct($sum, User $user)
    {
        $this->sum  = $sum;
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getSum(): int
    {
        return $this->sum;
    }



}