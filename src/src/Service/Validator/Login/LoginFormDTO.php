<?php

namespace App\Service\Validator\Login;

class LoginFormDTO
{
    private $login;
    private $password;

    /**
     * LoginFormDTO constructor.
     * @param $login
     * @param $password
     */
    public function __construct($login, $password)
    {
        $this->login    = $login;
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

}