<?php

namespace App\Service\Validator\Login;

class LoginFormValidator
{
    private $errors = [];

    public function validate(array $data): ?LoginFormDTO
    {
        if (empty($data['login'])) {
            $this->errors['login'] = 'Empty login';
        }
        if (empty($data['password'])) {
            $this->errors['password'] = 'Empty password';
        }

        if (empty($this->errors)) {
            return new LoginFormDTO($data['login'], $data['password']);
        }

        return null;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

}