<?php

namespace App\Repository;

use App\Entity\User;
use App\Service\Database\PdoFactory;
use App\Service\Factory\UserFactory;
use Exception;
use PDO;

class UserRepository implements UserRepositoryInterface
{

    /**
     * @var PDO
     */
    private $pdo;
    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * UserRepository constructor.
     * @param UserFactory $userFactory
     */
    public function __construct(UserFactory $userFactory)
    {
        $this->pdo         = PdoFactory::getPdo();
        $this->userFactory = $userFactory;
    }

    public function getUserById(int $userId): ?User
    {
        $stmt = $this->pdo->prepare('SELECT * FROM `user` WHERE `id`=?');
        $stmt->execute([$userId]);
        $data = $stmt->fetch();
        if (!$data) {
            return null;
        }

        return $this->userFactory->createFromArray($data);
    }

    public function getUserByLogin(string $login): ?User
    {
//        $stmt = $this->pdo->prepare('INSERT INTO user (`login`,`password`) VALUES (?,?)');
//        $password = password_hash(trim('demo'), PASSWORD_DEFAULT);
//        $stmt->execute(['demo',$password]);

        $stmt = $this->pdo->prepare('SELECT * FROM `user` WHERE `login` = ?');
        $stmt->execute([$login]);
        $data = $stmt->fetch();
        if (!$data) {
            return null;
        }

        return $this->userFactory->createFromArray($data);
    }

    /**
     * @param User $user
     * @param float $sum
     * @throws Exception
     */
    public function makeTransaction(User $user, float $sum)
    {
        try {
            $this->pdo->beginTransaction();
            $stmt = $this->pdo->prepare("SELECT `balance` FROM `user` WHERE id = ? FOR UPDATE");
            $stmt->execute([$user->getId()]);
            $data = $stmt->fetch();
            if ((float)$data['balance'] < $sum) {
                throw new Exception('Balance less then transaction sum');
            }
            $stmt = $this->pdo->prepare("UPDATE user SET balance = ? WHERE id = ?");
            $stmt->execute([
                    (float)$data['balance'] - $sum,
                    $user->getId()
                ]
            );
            $this->pdo->commit();
        } catch (Exception $e) {
            $this->pdo->rollBack();
            throw  $e;
        }
    }
}