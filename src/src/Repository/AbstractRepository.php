<?php

namespace App\Repository;

use App\Entity\User;

abstract class AbstractRepository
{

    protected $pdo;

    /**
     * AbstractRepository constructor.
     */
    public function __construct()
    {

    }

    public abstract function getUserById(int $userId): ?User;

    public abstract function updateUserBalance(User $user, float $balance): bool;

}