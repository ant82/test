<?php

namespace App\Repository;

use App\Entity\User;

interface UserRepositoryInterface
{
    public function getUserById(int $userId): ?User;

    public function makeTransaction(User $user, float $sum);
}