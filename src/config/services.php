<?php

use App\Repository\UserRepository;
use App\Repository\UserRepositoryInterface;
use Psr\Container\ContainerInterface;

return [

    UserRepositoryInterface::class => function (ContainerInterface $c) {
        return $c->get(UserRepository::class);
    },
];