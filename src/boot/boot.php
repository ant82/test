<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
$builder = new DI\ContainerBuilder();
$builder->addDefinitions(include_once(__DIR__ . '/../config/services.php'));
$container = $builder->build();
$request   = Request::createFromGlobals();
$session   = new Session();
$session->start();
$request->setSession($session);
$container->set(Request::class, $request);
$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../templates');
//$twig   = new \Twig\Environment($loader, [
//    'cache' => __DIR__ . '/../tmp',
//]);
$twig = new \Twig\Environment($loader, []);
$container->set(\Twig\Environment::class, $twig);

return $container;